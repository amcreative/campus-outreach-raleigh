<?php

namespace CORaleigh;

use CORaleigh\Theme;

require_once 'vendor/autoload.php';

add_action( 'after_setup_theme', function() {
	global $CORaleigh;

	if ( ! empty( $CORaleigh ) ) {
		return;
	}

	if ( ! defined( 'CO_RALEIGH_VERSION' ) ) {
		define( 'CO_RALEIGH_VERSION', '1.0.0' );
	}
	if ( ! defined( 'CO_RALEIGH_URL' ) ) {
		define( 'CO_RALEIGH_URL', get_stylesheet_directory_uri() );
	}
	if ( ! defined( 'CO_RALEIGH_TEMPLATE_URL' ) ) {
		define( 'CO_RALEIGH_TEMPLATE_URL', get_template_directory_uri() );
	}
	if ( ! defined( 'CO_RALEIGH_PATH' ) ) {
		define( 'CO_RALEIGH_PATH', trailingslashit( get_stylesheet_directory() ) );
	}
	if ( ! defined( 'CO_RALEIGH_INC' ) ) {
		define( 'CO_RALEIGH_INC', CO_RALEIGH_PATH . 'includes/' );
	}

	$CORaleigh = new Theme();
});

