import Power1 from 'gsap/umd/TweenMax';
import * as utils from './Utils';

class PrimaryNavigation {

    activeClass = '-active';
    body = document.body;
    btn = document.getElementById('js-navigation-toggle-button');
    duration = .150;
    isActive = false;
    lastFocus = null;
    menu = document.getElementById('js-main-navigation');
    openClass = 'nav-open';

    /**
     * The PrimaryNavigation Cosntructor.
     */
    construct() {
        this.activateNav = this.activateNav.bind(this);
        this.deactivateNav = this.deactivateNav.bind(this);
        this.openNav = this.openNav.bind(this);
        this.closeNav = this.closeNav.bind(this);
        this.triggerClick = this.triggerClick.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.init = this.init.bind(this);
    }

    /**
     * Method to return a promise resolving the active state of the nav.
     *
     * @returns {Promise<any>}
     */
    activateNav() {
        return new Promise((resolve, reject) => {
            this.isActive = true;
            resolve(this.isActive);
        });
    }

    /**
     * Method to return a promise resolving the inactive state of the nav.
     *
     * @returns {Promise<any>}
     */
    deactivateNav() {
        return new Promise((resolve, reject) => {
            this.isActive = false;
            resolve(this.isActive);
        });
    }

    /**
     * Method to open the booking navigation.
     *
     * @returns {void}
     */
    openNav() {
        const winW = utils.getWinWidth(),
            startPos = winW >= 768 ? -320 : -1000,
            fromToOpts = {
              el: this.menu,
              dur: this.duration,
              from: {
                left:startPos,
                opacity:0
              },
              to: {
                left:0,
                opacity:1
              }
            };;

        if (true === this.isActive) {
            return false;
        }

        this.activateNav()
            .then((fulfilled) => {
                this.lastFocus = document.activeElement;
            })
            .then(() => {
                this.menu.setAttribute('aria-hidden', 'false');
            })
            .then(() => {
                utils.addClass(this.menu, this.activeClass);
            })
            .then(() => {
                utils.addClass(this.btn, this.activeClass);
                setTimeout(() => {
                    utils.animateTo(this.btn, this.duration, {backgroundColor:'#fff', delay:.05, ease:Power1.easeInOut});
                }, 100);
            })
            .then(() => {
                this.body.classList.add(this.openClass);
            })
            .then(() => {
              utils.animateFromTo(fromToOpts);
            })
            .then(() => {
                this.menu.focus();
            });
    }

    /**
     * Method to close the navigation.
     *
     * @param {object} e the event Object.
     * @returns {void}
     */
    closeNav() {
        const winW = utils.getWinWidth(),
            endPos = winW >= 768 ? -320 : -1000,
            fromToOpts = {
              el: this.menu,
              dur: this.duration,
              from: {
                left:0,
                opacity:1
              },
              to: {
                left:endPos,
                opacity:0
              }
            };

        if (false === this.isActive) {
            return;
        }

        this.deactivateNav()
            .then((fulfilled) => {
              utils.animateFromTo(fromToOpts);
            })
            .then(() => {
                this.body.classList.remove(this.openClass);
            })
            .then(() => {
                utils.removeClass(this.menu, this.activeClass);
            })
            .then(() => {
                utils.removeClass(this.btn, this.activeClass);
                setTimeout(() => {
                    utils.animateTo(this.btn, this.duration, {backgroundColor:'transparent', delay:.025, ease:Power1.easeInOut});
                }, 100);
            })
            .then(() => {
                this.menu.setAttribute('aria-hidden', 'true');
            })
            .then(() => {
                this.menu.setAttribute('tabindex', '-1');
            })
            .then(() => {
                this.lastFocus.focus();
            })
            .then(() => {
                this.lastFocus = null;
            });
    }

    /**
     * Method to triggle the click event.
     *
     * @returns {void}
     */
    triggerClick() {
        if (false === this.isActive) {
            this.openNav();
        } else {
            this.closeNav();
        }
    }

    /**
     * Method to handle close the navigation when the `esc` key is clicked.
     *
     * @param {object} e the event Object.
     * @returns {void}
     */
    handleKeyDown(e) {
        const escKey = 27,
            key = e.keyCode;

        if (escKey !== key && false === this.isActive) {
            return;
        }

        this.closeNav();
    }

    /**
     * Method to initialize the primary navigation actions.
     *
     * @returns {void}
     */
    init() {
        this.btn.addEventListener('click', this.triggerClick.bind(this));
        document.addEventListener('keyup', this.handleKeyDown.bind(this), false);
    }
};

export default PrimaryNavigation;
