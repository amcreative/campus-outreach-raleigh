import Power1 from 'gsap/umd/TweenMax';
import * as utils from './Utils';
import Headroom from 'headroom.js';

/**
 * Class representing header animation.
 */
class HeaderAnimation {

  duration = .150;
  header = document.getElementById('js-sticky-header');
  hero = document.getElementById('js-hero-container');
  logoCOShape = document.getElementById('js-logo-co-shape');
  logoGlobe = document.getElementById('js-logo-globe');
  logoLocation = document.getElementById('js-logo-location');
  logoTitle = document.getElementById('js-logo-title');

  /**
   * The HeaderAnimation Constructor.
   *
   * @returns {void}
   */
  construct() {
    this.setSticky = this.setSticky.bind(this);
    this.init = this.init.bind(this);
  }

  /**
   * Method to set the properties of the sticky header.
   *
   * @returns {void}
   */
  setStickyProps() {
    utils.animateTo(this.header, this.duration, {backgroundColor:'#fff', borderBottomWidth:1, ease:Power1.easeInOut});
    utils.animateTo(this.logoCOShape, this.duration, {fill:'rgba(57, 56, 57, 1)', ease:Power1.easeInOut});
    utils.animateTo(this.logoGlobe, this.duration, {fill:'rgba(0, 134, 193, 1)', ease:Power1.easeInOut});
    utils.animateTo(this.logoLocation, this.duration, {fill:'rgba(57, 56, 57, 1)', ease:Power1.easeInOut});
    utils.animateTo(this.logoTitle, this.duration, {fill:'rgba(0, 134, 193, 1)', ease:Power1.easeInOut});
  }

  /**
   * Method to set the initial properties of the sticky header.
   *
   * @returns {void}
   */
  setInitialProps() {
    utils.animateTo(this.header, this.duration, {backgroundColor:'transparent', borderBottomWidth:0, ease:Power1.easeInOut});
    utils.animateTo(this.logoCOShape, this.duration, {fill:'rgba(255, 255, 255, 1)', ease:Power1.easeInOut});
    utils.animateTo(this.logoGlobe, this.duration, {fill:'rgba(255, 255, 255, 1)', ease:Power1.easeInOut});
    utils.animateTo(this.logoLocation, this.duration, {fill:'rgba(255, 255, 255, 1)', ease:Power1.easeInOut});
    utils.animateTo(this.logoTitle, this.duration, {fill:'rgba(255, 255, 255, 1)', ease:Power1.easeInOut});
  }

  /**
   * Method to set the sticky state.
   *
   * @returns {void}
   */
  setStickyState() {

    if (this.hero) {
      utils.addClass(this.hero, '-sticky');
      this.setStickyProps();
    }
  }

  /**
   * Method to set the initial state.
   *
   * @returns {void}
   */
  setInitialState() {

    if (this.hero) {
      utils.removeClass(this.hero, '-sticky');
      this.setInitialProps();
    }
  }

  /**
   * Method to handle scroll events for the header.
   *
   * @returns {void}
   */
  handleScroll() {
    const winW = utils.getWinWidth(),
      opts = {
        tolerance: {
          down: 0,
          up: 0
        },
        offset: winW >= 768 ? 71 : 51,
        classes: {
          initial: 'sticky-header',
          top: '-top',
          notTop: '-nottop',
          pinned: '-pinned',
          unpinned: '-unpinned',
          bottom: '-bottom',
          notBottom: '-notbottom'
        },
        onNotTop: () => {
          this.setStickyState();
        },
        onTop: () => {
          this.setInitialState();
        }
      },
      headroom = new Headroom(this.header, opts);

    headroom.init();
  }

  /**
   * Method to initialize the sticky header functionality.
   *
   * @returns {void}
   */
  init() {
    this.handleScroll();
  }
};

export default HeaderAnimation;
