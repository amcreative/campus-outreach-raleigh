import * as utils from './Utils';

class StaffFilter {

  allFilterBtn = document.getElementById('js-staff-filter-all');
  csFilterBtn = document.getElementById('js-staff-filter-cs');
  cuFilterBtn = document.getElementById('js-staff-filter-cu');
  ecuFilterBtn = document.getElementById('js-staff-filter-ecu');
  etFilterBtn = document.getElementById('js-staff-filter-et');
  visibleClass = '-isvisible';
  moblFilterBtn = document.getElementById('js-staff-filter-mobl');
  ncsuFilterBtn = document.getElementById('js-staff-filter-ncsu');
  roFilterBtn = document.getElementById('js-staff-filter-ro');
  uncchFilterBtn = document.getElementById('js-staff-filter-uncch');

  /**
   * The StaffFilter Constructor.
   */
  construct() {
    this.getAllTeams = this.getAllTeams.bind(this);
    this.getAllFilterBtns = this.getAllFilterBtns.bind(this);
    this.getFilterId = this.getFilterId.bind(this);
    this.getFilterTerm = this.getFilterTerm.bind(this);
    this.getFilterResults = this.getFilterResults.bind(this);
    this.filterResults = this.filterResults.bind(this);
    this.displayFilterResults = this.displayFilterResults.bind(this);
    this.handleClickEvents = this.handleClickEvents.bind(this);
    this.init = this.init.bind(this);
  }

  getAllTeams() {
    const teams = document.getElementsByClassName('team-list');

    return teams;
  }

  getAllFilterBtns() {
    const btns = document.getElementsByClassName('filter-button');

    return btns;
  }

  getFilterId(el) {
    const node = el,
      filterId = node.dataset.fid;

    return filterId;
  }

  getFilterTerm(el) {
    const node = el,
      filterTerm = node.dataset.filter;

    return filterTerm;
  }

  getFilterResults(el) {
    const node = el,
      term = this.getFilterTerm(node);
    let filter;

    filter = document.getElementsByClassName(term);

    return filter;
  }

  filterResults(obj) {
    const team = obj,
      id = this.getFilterId(team),
      term = `-${team}`;

    if (id = team.getAttribute('data-fid') && !team.classList.contains(this.visibleClass)) {
      utils.removeClass(term, this.visibleClass);
    } else {
      utils.addClass(term, this.visibleClass);
    }
  }

  displayFilterResults(e) {
    const node = e.target,
      filter = this.getFilterResults(node),
      teams = this.getAllTeams();
    let result,
      team;

    for (let i = 0, len = teams.length; i < len; i++) {
      team = teams[i];
      this.filterResults(team);
    }
  }

  handleClickEvents() {
    const btns = this.getAllFilterBtns();
    let btn;

    for (let i = 0, len = btns.length; i < len; i++) {
      btn = btns[i];
      btn.addEventListener('click', this.displayFilterResults.bind(this));
    }
  }

  /**
   * Method to initialize the staff filter methods.
   */
  init() {
    this.handleClickEvents();
  }
}

export default StaffFilter;
