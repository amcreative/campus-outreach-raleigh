import GlobalSearch from './GlobalSearch';
import HeaderAnimation from './HeaderAnimation';
import PrimaryNavigation from './PrimaryNavigation';
import SetHeaderHeight from './SetHeaderHeight';
import StaffFilter from './StaffFilter';

const gs = new GlobalSearch(),
  ha = new HeaderAnimation(),
  pn = new PrimaryNavigation(),
  shh = new SetHeaderHeight(),
  sf = new StaffFilter();

gs.init();
ha.init();
pn.init();
shh.init();
sf.init();
