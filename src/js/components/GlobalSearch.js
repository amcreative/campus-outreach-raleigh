import Power1 from 'gsap/umd/TweenMax';
import * as utils from './Utils';

class GlobalSearch {

  body = document.body;
  closeClass = 'modal-close';
  curModal = null;
  curOverlay = null;
  duration = .150;
  lastFocus = null;
  modalActive = false;
  modalActiveEvent = new Event('cosmodalactive');
  overlay = document.getElementById('js-modal-overlay');
  overlayActive = false;
  wrapperClass = 'modal-wrapper';

  /**
   * The GlobalSearch Constructor.
   */
  construct() {
    this.setState = this.setState.bind(this);
    this.createOverlay = this.createOverlay.bind(this);
    this.deleteOverlay = this.deleteOverlay.bind(this);
    this.openSearch = this.openSearch.bind(this);
    this.closeSearch = this.closeSearch.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.closeByWrapper = this.closeByWrapper.bind(this);
    this.closeByBtn = this.closeByBtn.bind(this);
    this.triggerModal = this.triggerModal.bind(this);
    this.init = this.init.bind(this);
  }

  /**
   * Method to return a promise setting an object's state.
   *
   * @returns {Promise<any>}
   */
  setState(state) {
    return new Promise((resolve, reject) => {
      resolve(state);
    });
  }

  /**
   * Method to add a modal overlay to the DOM.
   *
   * @returns {void}
   */
  createOverlay() {
    const overlay = document.getElementById('js-modal-overlay');

    if (true === this.overlayActive) {
      return;
    }

    this.setState(this.overlayActive = true)
      .then((fulfilled) => {
        this.curOverlay = overlay;
      })
      .then(() => {
        utils.animateTo(this.curOverlay, this.duration, { opacity: '.95', ease: Power1.easeInOut });
      })
      .then(() => {
        this.curOverlay.setAttribute('aria-hidden', 'false');
      })
      .then(() => {
        this.curOverlay.setAttribute('tab-index', '-1');
      })
      .then(() => {
        this.body.classList.add('modal-visible');
      });
  }

  /**
   * Method to remove the modal overlay to the DOM.
   *
   * @returns {void}
   */
  deleteOverlay() {
    if (false === this.overlayActive) {
      return;
    }

    this.setState(this.overlayActive = false)
      .then((fulfilled) => {
        utils.animateTo(this.curOverlay, this.duration, { opacity: '0', ease: Power1.easeInOut });
      })
      .then(() => {
        this.body.classList.remove('modal-visible');
      })
      .then(() => {
        this.curOverlay.setAttribute('aria-hidden', 'true');
      })
      .then(() => {
        this.curOverlay = null;
      });
  }

  /**
   * Method to open the booking modal.
   *
   * @returns {void}
   */
  openSearch() {
    const node = document.getElementById('search-modal');

    if (true === this.modalActive) {
      return false;
    }

    this.setState(this.modalActive = true)
      .then((fulfilled) => {
        this.lastFocus = document.activeElement;
      })
      .then(() => {
        document.dispatchEvent(this.modalActiveEvent);
      })
      .then(() => {
        this.curModal = node;
      })
      .then(() => {
        this.createOverlay();
      })
      .then(() => {
        utils.animateTo(this.curModal, this.duration, { opacity: '1', ease: Power1.easeInOut });
      })
      .then(() => {
        this.curModal.setAttribute('aria-hidden', 'false');
      })
      .then(() => {
        this.curModal.setAttribute('tabindex', '0');
      })
      .then(() => {
        this.curModal.focus();
      });
  }

  /**
   * Method to close the modal.
   *
   * @param {object} e the event Object,.
   * @returns {void}
   */
  closeSearch() {
    if (false === this.modalActive) {
      return false;
    }

    this.setState(this.modalActive = false)
      .then((fulfilled) => {
        utils.animateTo(this.curModal, this.duration, { opacity: '0', ease: Power1.easeInOut });
      })
      .then(() => {
        this.deleteOverlay();
      })
      .then(() => {
        this.curModal.setAttribute('aria-hidden', 'true');
      })
      .then(() => {
        this.curModal.setAttribute('tabindex', '-1');
      })
      .then(() => {
        this.curModal = null;
      })
      .then(() => {
        this.lastFocus.focus();
      })
      .then(() => {
        this.lastFocus = '';
      });
  }

  /**
   * Method to handle close the modal when the `esc` key is clicked.
   *
   * @param {object} e the event Object.
   * @returns {void}
   */
  handleKeyDown(e) {
    const escKey = 27,
      key = e.keyCode;

    if (escKey !== key && false === this.modalActive) {
      return false;
    }
    this.closeSearch();
  }

  /**
   * Method to close all modals when a modal wrapper is clicked.
   *
   * @returns {void}
   */
  closeByWrapper() {
    const els = document.getElementsByClassName(this.wrapperClass);
    let el;

    for (let i = 0, len = els.length; i < len; i++) {
      el = els[i];
      el.addEventListener('click', this.closeSearch.bind(this));
    }
  }

  /**
   * Method to close all modals when the close button is clicked.
   *
   * @returns {void}
   */
  closeByBtn() {
    const btn = document.getElementById('js-close-modal');

    btn.addEventListener('click', this.closeSearch.bind(this), false);
  }

  /**
   * Method to close all modals when the close button is clicked.
   *
   * @returns {void}
   */
  triggerModal() {
    const btn = document.getElementById('js-search-toggle-button');

    btn.addEventListener('click', this.openSearch.bind(this), false);
  }

  /**
   * Method to initialize the primary navigation actions.
   */
  init() {
    this.triggerModal();
    this.closeByWrapper();
    this.closeByBtn();
  }
}

export default GlobalSearch;
