import * as utils from './Utils';

/**
 * Class representing SetHeaderHeight.
 */
class SetHeaderHeight {
  /**
   * The SetHeaderHeight Constructor.
   *
   * @returns {void}
   */
  construct() {
    this.setValues = this.setValues.bind(this);
    this.getRemVal = this.getRemVal.bind(this);
    this.setHeightVal = this.setHeightVal.bind(this);
    this.setHeight = this.setHeight.bind(this);
    this.init = this.init.bind(this);
  }

  /**
   * Method to set final height value.
   *
   * @param {array} opts the height arguments.
   * @returns {number} the final height value.
   */
  setHeightVal(opts) {
    const ab = opts.ab,
      header = opts.raw,
      h = header - ab;

    return h;
  }

  /**
   * Method to set the height as rem units.
   *
   * @param {array} opts the height arguments.
   * @returns {string} the height as a rem unit.
   */
  getRemVal(opts) {
    const val = this.setHeightVal(opts),
      h = val / 16,
      remH = `${h}rem`;

    return remH;
  }

  /**
   * Method to set the height values.
   *
   * @returns {void}
   */
  setValues() {
    const h = {
        ab: utils.getAdminBarHeight(),
        raw: utils.getWinHeight()
      },
      remH = this.getRemVal(h);

    return remH;
  }

  /**
   * Method to get the inline styles of the header.
   *
   * @param {string} el the element to get properties of..
   * @returns {string} the inline styles.
   */
  getCurrentStyles(el) {
    const props = el.getAttribute('style');

    return props;
  }

  /**
   * Method to set the hero height.
   *
   * @param {string} el the element to set properties for.
   * @returns {void}
   */
  setHeight(el) {
    const props = this.getCurrentStyles(el),
      h = this.setValues();

    el.setAttribute('style', `${props}); height: ${h}`);
  }

  /**
   * Method to initialize the Hero Height functions.
   *
   * @returns {void}
   */
  init() {
    const header = document.getElementById('js-site-header');

    if (false === utils.isFrontPage() || header.classList.contains('-staff')) {
      return;
    }

    this.setHeight(header);
  }
}

export default SetHeaderHeight;
