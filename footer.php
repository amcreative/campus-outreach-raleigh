<?php
/**
 * Fires after the main content, before the footer is output.
 *
 * @package CORaleigh
 * @since 1.0.0
 */

$postTypes = array(
	'staff',
	'tribe_events'
);

if ( is_singular( $postTypes ) ) { ?>
	</main>
<?php }

do_action( 'et_after_main_content' );

if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

		if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer class="site-footer">
				<div class="container-row">
					<div class="footer-colophon" itemscope itemtype="http://schema.org/Organization">
						<h2 class="footer-title" itemprop="name"><?php bloginfo( 'name' ); ?></h2>
						<div class="colophon-block -description">
							<?php esc_html_e( 'We\'re simple. We believe God\'s call on each person\'s life is to love Christ and be on mission for him. As an organization, our aim is to help college students learn how to do that.', 'co-raleigh' ); ?>
						</div>
						<div class="colophon-block -address">
							<?php inline_svg( 'map-marker' ); ?>
							<address class="footer-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div class="street-address" itemprop="streetAddress">
									<?php esc_html_e( '2335 Presidential Drive', 'co-raleigh' ); ?>
								</div>
								<div class="address-locale">
									<span class="city-state" itemprop="addressLocality"><?php esc_html_e( 'Durham, NC', 'co-raleigh' ); ?></span>
									<span class="postal-code" itemprop="postalCode"><?php esc_html_e( '27703', 'co-raleigh' ); ?></span>
								</div>
							</address>
						</div>
						<div class="colophon-block -phone" itemprop="telephone">
							<?php inline_svg( 'phone' ); ?>
							<div class="footer-phone">
								<?php esc_html_e( '(919) 354-5991', 'co-raleigh' ); ?>
							</div>
						</div>
						<div class="colophon-block -email" itemprop="email">
							<?php inline_svg( 'envelope' ); ?>
							<div class="footer-email">
								<?php esc_html_e( 'raleigh@campusoutreach.org', 'co-raleigh' ); ?>
							</div>
						</div>
					</div>
					<div class="footer-contact">
						<h2 class="footer-title"><?php esc_html_e( 'Contact Us', 'co-raleigh' ); ?></h2>
						<div class="footer-form">
							<?php echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true"]' ); ?>
						</div>
					</div>
				</div>
			</footer>
		<?php endif;
		do_action( 'corSearchModal' );
		wp_footer(); ?>
	</body>
</html>
