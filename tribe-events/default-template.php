<?php
/**
 * Default Events Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header( 'event' );
?>
<main class="site-content">
	<div class="container-row">
		<?php tribe_events_before_html(); ?>
		<?php tribe_get_view(); ?>
		<?php tribe_events_after_html(); ?>
	</div>
</main>
<?php
get_footer();
