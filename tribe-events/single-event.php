<?php
/**
 * CO Raleigh: Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * @package CORaleigh
 * @version 1.0.0
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$eventsLabelSingular = tribe_get_event_label_singular();
$eventsLabelPlural   = tribe_get_event_label_plural();

while ( have_posts() ) :  the_post(); ?>

	<div class="container-column">

		<!-- Event content -->
		<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>

		<section class="entry-content">
			<?php the_content(); ?>
		</section>
	</div>

<?php endwhile; ?>
