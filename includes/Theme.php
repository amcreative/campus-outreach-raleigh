<?php

namespace CORaleigh;

use CORaleigh\Admin\Options;
use CORaleigh\Modules\Content\Events\FeaturedEvents;
use CORaleigh\Modules\Divi\Filters\BCWrap;
use CORaleigh\Modules\GravityForms\Fields as GFFields;
use CORaleigh\Modules\Header\FavIcons;
use CORaleigh\Modules\Header\Fonts;
use CORaleigh\Modules\PostTypes\Staff as StaffCPT;
use CORaleigh\Modules\PostTypes\Meta\Staff as StaffMeta;
use CORaleigh\Modules\Search\SiteSearch;
use CORaleigh\Modules\Taxonomy\StaffTeams;

class Theme {

	/**
	 * Property representing the Options class.
	 *
	 * @var \CORaleigh\Admin\Options
	 */
	public $options;

	/**
	 * Property representing the FeaturedEvents class.
	 *
	 * @var \CORaleigh\Modules\Content\Events\FeaturedEvents
	 */
	public $feShortcode;

	/**
	 * Property representing the BCWrap class.
	 *
	 * @var \CORaleigh\Modules\Divi\Filters\BCWrap
	 */
	public $diviBCWrap;

	/**
	 * Property representing the Fields class.
	 *
	 * @var \CORaleigh\Modules\GravityForms\Fields
	 */
	public $gfFields;

	/**
	 * Property representing the FavIcons class.
	 *
	 * @var \CORaleigh\Modules\Header\FavIcons
	 */
	public $favIcons;

	/**
	 * Property representing the Fonts class.
	 *
	 * @var \CORaleigh\Modules\Header\Fonts
	 */
	public $themeFonts;

	/**
	 * Property representing the Staff cpt class.
	 *
	 * @var \CORaleigh\Modules\PostTypes\Staff
	 */
	public $staffCpt;

	/**
	 * Property representing the Staff meta class.
	 *
	 * @var \CORaleigh\Modules\PostTypes\Meta\Staff
	 */
	public $staffMeta;

	/**
	 * Property representing the SiteSearch class.
	 *
	 * @var \CORaleigh\Modules\Search\SiteSearch
	 */
	public $siteSearch;

	/**
	 * Property representing the StaffTeams class.
	 *
	 * @var \CORaleigh\Modules\Taxonomy\StaffTeams
	 */
	public $staffTeams;

	/**
	 * The Theme Constructor.
	 */
	public function __construct() {
		global $content_width;

		if ( ! isset( $content_width ) ) {
			$content_width = 1200;
		}

		$this->options = new Options();
		$this->feShortcode = new FeaturedEvents();
		$this->bcClasses = new BCWrap();
		$this->gfFields = new GFFields();
		$this->favIcons = new FavIcons();
		$this->themeFonts = new Fonts();
		$this->staffCpt = new StaffCPT();
		$this->staffMeta = new StaffMeta();
		$this->siteSearch = new SiteSearch();
		$this->staffTeams = new StaffTeams();

		$this->setupL10n();
		$this->registerThemeMenus();
		$this->registerImageSizes();

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueueScripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueueParentStyles' ), 9 );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueueStyles' ) );

		add_filter( 'body_class', array( $this, 'addBodyClass' ) );
		add_action( 'wp_head', array( $this, 'adjustHtmlHeight' ), 100 );
	}

	/**
	 * Method to setup the textdomain.
	 */
	public function setupL10n() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'co-raleigh' );
		load_textdomain( 'co-raleigh', WP_LANG_DIR . '/co-raleigh/co-raleigh-' . $locale . '.mo' );
		load_theme_textdomain( 'co-raleigh', CO_RALEIGH_PATH . '/languages' );
	}

	/**
	 * Method to enqueue the front end JavaScript.
	 */
	public function enqueueScripts() {
		$min = defined( 'SCRIPT_DEBUG' ) && filter_var( SCRIPT_DEBUG, FILTER_VALIDATE_BOOLEAN ) ? '' : '.min';
		$themeUrl = trailingslashit( CO_RALEIGH_URL );

		wp_register_script(
			'web-animations-js',
			$themeUrl . 'dist/js/vendor/web-animations-next.min.js',
			[],
			'2.3.1',
			true
		);

		wp_enqueue_script(
			'co-js',
			$themeUrl . 'dist/js/build' . $min . '.js',
			array(
				'web-animations-js',
			),
			CO_RALEIGH_VERSION,
			true
		);

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	/**
	 * Method to enqueue the parent theme styles.
	 */
	public function enqueueParentStyles() {
		$parentStyle = 'divi-style';
		$parentUrl = trailingslashit( CO_RALEIGH_TEMPLATE_URL );

		wp_enqueue_style(
			$parentStyle,
			$parentUrl . 'style.css'
		);
	}

	/**
	 * Method to enqueue the front end CSS.
	 */
	public function enqueueStyles() {
		$min = defined( 'SCRIPT_DEBUG' ) && filter_var( SCRIPT_DEBUG, FILTER_VALIDATE_BOOLEAN ) ? '' : '.min';
		$parentStyle = 'divi-style';
		$themeUrl = trailingslashit( CO_RALEIGH_URL );

		wp_enqueue_style(
			'co-raleigh',
			$themeUrl . 'dist/css/style.css',
			array(
				$parentStyle
			),
			CO_RALEIGH_VERSION,
			'all'
		);
	}

	/**
	 * Method to register theme menus.
	 */
	public function registerThemeMenus() {
		register_nav_menus(['social-navigation' => __( 'Social Navigation', 'co-raleigh' )]);
	}

	/**
	 * Method to register custom image sizes.
	 */
	public function registerImageSizes() {
		add_image_size( 'full-width-hero', 1920, 9999, false );
		add_image_size( 'staff-headshot', 600, 600, array( 'center', 'top' ) );
	}

	/**
	 * Method to add a new body class.
	 *
	 * @param array $classes collection of classes to be added to the body tag
	 * @return array the filtered array of body classes
	 */
	public function addBodyClass( $classes ) {
		if ( ! is_admin() ) {
			$classes[] = 'site-body';
		}

		return $classes;
	}

	/**
	 * Method to adjust the height of the html document tag.
	 */
	public function adjustHtmlHeight() {
		if ( false === is_user_logged_in() ) {
			return;
		}
		?>
		<style type="text/css" media="screen">
			html {
				height: calc(100% - 46px);
			}

			@media screen and ( min-width: 783px ) {
				html {
					height: calc(100% - 32px);
				}
			}
		</style>
		<?php
	}
}
