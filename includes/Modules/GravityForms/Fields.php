<?php

namespace CORaleigh\Modules\GravityForms;

class Fields {

	/**
	 * The Fields Constructor.
	 */
	public function __construct() {
		add_filter( 'gform_field_content', array( $this, 'inputField' ), 10, 2 );
		add_filter( 'gform_field_content', array( $this, 'selectField' ), 10, 2 );
		add_filter( 'gform_field_content', array( $this, 'textareaField' ), 10, 2 );
		add_filter( 'gform_submit_button', array( $this, 'submitButton' ), 10, 2 );
	}

	/**
	 * Method to add a custom CSS class to a Gravity Forms input field.
	 *
	 * @param string $content the field content to be filtered.
	 * @param object $field the field the content applies to.
	 * @return string the filtered content.
	 */
	public function inputField( $content, $field ) {

		$field_types = [ 'text', 'number', 'name', 'phone', 'website', 'email' ];

		foreach ( $field_types as $type ) {
			if ( $field->type === $type ) {
				return str_replace( "class='large'", "class='input-field'", $content );
			}
		}
		return $content;
	}

	/**
	 * Method to add a custom CSS class to a Gravity Forms select field.
	 *
	 * @param string $content the field content to be filtered.
	 * @param object $field the field the content applies to.
	 * @return string the filtered content.
	 */
	public function selectField( $content, $field ) {

		if ( $field->type === 'select' ) {
			return str_replace( "class='large gfield_select'", "class='select-box'", $content );
		}
		return $content;
	}

	/**
	 * Method to add a custom CSS class to a Gravity Forms textarea field.
	 *
	 * @param string $content the field content to be filtered.
	 * @param object $field the field the content applies to.
	 * @return string the filtered content.
	 */
	public function textareaField( $content, $field ) {

		if ( $field["type"] === 'textarea' ) {
			$content = str_replace('class=\'textarea large', 'class=\'textarea-field', $content );
		}

		return $content;
	}
	/**
	 * Method to replace the submit button with a `<button>` element with a custom CSS class.
	 *
	 * @param string $button the tag string to be filtered.
	 * @param object $form the form currently being processed.
	 * @return string the filtered tag string.
	 */
	public function submitButton( $button, $form ) {

		$button = sprintf( '<button class="submit-button" id="gform_submit_button_%s">%s</button>', esc_attr( $form['id'] ), esc_html( $form['button']['text'] ) );

		return $button;
	}
}
