<?php

namespace CORaleigh\Modules\Taxonomy;

class StaffTeams {

	/**
	 * The StaffTeams Constructor.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'registerTaxonomy' ), 0 );
	}

	/**
	 * Method that returns the taxonomy labels.
	 *
	 * @return array an array of labels.
	 */
	public function getLabels() {

		$labels = array(
			'name'                       => _x( 'Teams', 'Taxonomy General Name', 'co-raleigh' ),
			'singular_name'              => _x( 'Team', 'Taxonomy Singular Name', 'co-raleigh' ),
			'menu_name'                  => __( 'Teams', 'co-raleigh' ),
			'all_items'                  => __( 'All Items', 'co-raleigh' ),
			'parent_item'                => __( 'Parent Item', 'co-raleigh' ),
			'parent_item_colon'          => __( 'Parent Item:', 'co-raleigh' ),
			'new_item_name'              => __( 'New Item Name', 'co-raleigh' ),
			'add_new_item'               => __( 'Add New Item', 'co-raleigh' ),
			'edit_item'                  => __( 'Edit Item', 'co-raleigh' ),
			'update_item'                => __( 'Update Item', 'co-raleigh' ),
			'view_item'                  => __( 'View Item', 'co-raleigh' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'co-raleigh' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'co-raleigh' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'co-raleigh' ),
			'popular_items'              => __( 'Popular Items', 'co-raleigh' ),
			'search_items'               => __( 'Search Items', 'co-raleigh' ),
			'not_found'                  => __( 'Not Found', 'co-raleigh' ),
			'no_terms'                   => __( 'No items', 'co-raleigh' ),
			'items_list'                 => __( 'Items list', 'co-raleigh' ),
			'items_list_navigation'      => __( 'Items list navigation', 'co-raleigh' ),
		);

		return $labels;
	}

	/**
	 * Method that returns the taxonomy arguments.
	 *
	 * @return array an array of arguments.
	 */
	public function getArgs() {

		$labels = $this->getLabels();

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'show_in_rest'               => true,
			'rest_base'                  => 'staff-teams-api',
			'rest_controller_class'      => 'WPRESTStaffTeamsController',
		);

		return $args;
	}

	/**
	 * Method that registers the taxonomy.
	 */
	public function registerTaxonomy() {

		$args = $this->getArgs();

		register_taxonomy( 'staff-team', array( 'staff' ), $args );
	}
}
