<?php

namespace CORaleigh\Modules\PostTypes\Meta;

class Staff {

	/**
	 * The Staff constructor.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'addMetaBox') );
		add_action( 'save_post', array( $this, 'saveData' ) );
	}

	/**
	 * Method to add a meta box to the welcome videos post type.
	 */
	public function addMetaBox() {
		add_meta_box(
			'staff-meta',
			'Staff Member Details',
			array( $this, 'renderMetaBox' ),
			'staff',
			'normal',
			'high'
		);
	}

	/**
	 * Method to render the meta box.
	 *
	 * @param WP_Post $post the post object.
	 */
	public function renderMetaBox( $post ) {

		wp_nonce_field( 'staff_meta_boxes', '_staff_nonce', false );
		?>

		<label for="staff_giving_url"><?php esc_html_e( 'Giving URL:', 'co-raleigh' ); ?></label>

		<input type="text" name="staff_giving_url" id="staff_giving_url" class="widefat" value="<?php echo esc_attr( get_post_meta( $post->ID, 'staff_giving_url', true ) ); ?>">

		<p class="description">
			<?php esc_html_e( 'Enter the url for the staff members online giving page.', 'co-raleigh' ); ?>
		</p>

		<label for="staff_title"><?php esc_html_e( 'Title:', 'co-raleigh' ); ?></label>

		<input type="text" name="staff_title" id="staff_title" class="widefat" value="<?php echo esc_attr( get_post_meta( $post->ID, 'staff_title', true ) ); ?>">

		<p class="description">
			<?php esc_html_e( 'Enter the staff member\'s job title.', 'co-raleigh' ); ?>
		</p>
		<?php
	}

	/**
	 * Method to save the meta data.
	 *
	 * @param int $postId the post id.
	 */
	public function saveData( $postId ) {

		$doing_autosave = defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE;
		$valid_nonce = wp_verify_nonce( filter_input( INPUT_POST, '_staff_nonce' ), 'staff_meta_boxes' );
		$can_edit = current_user_can( 'edit_post', $postId );

		if ( $doing_autosave || ! $valid_nonce || ! $can_edit ) {
			return;
		}

		$givingUrl = sanitize_text_field( filter_input( INPUT_POST, 'staff_giving_url' ) );
		$jobTitle = sanitize_text_field( filter_input( INPUT_POST, 'staff_title' ) );

		update_post_meta( $postId, 'staff_giving_url', $givingUrl );
		update_post_meta( $postId, 'staff_title', $jobTitle );
	}
}
