<div id="js-bible-embed-container" class="bible-container">
	<biblia:bible layout="normal" resource="<?php echo esc_attr( $translation ); ?>" width="400" height="600" startingReference="<?php echo esc_attr( $ref ); ?>"></biblia:bible>
	<script src="//biblia.com/api/logos.biblia.js"></script>
	<script>logos.biblia.init();</script>
</div>