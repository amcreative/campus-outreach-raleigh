<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

	<div id="main-content">

		<?php if ( ! is_front_page() ) : ?>

			<div id="content-area" class="clearfix">
				<div class="container-column">
		<?php endif; ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php $classes = is_front_page() ? 'entry-content -frontpage' : 'entry-content'; ?>

							<div class="<?php echo esc_attr( $classes ); ?>">

								<?php the_content(); ?>

							</div> <!-- .entry-content -->

						</article> <!-- .et_pb_post -->

					<?php endwhile; ?>

					<?php if ( ! is_front_page() ) : ?>
				</div>
			</div> <!-- #content-area -->

	<?php endif; ?>

	</div> <!-- #main-content -->

<?php

get_footer();
