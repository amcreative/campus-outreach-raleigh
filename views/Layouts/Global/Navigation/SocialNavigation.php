<?php

$socialWalker = new \CORaleigh\Modules\NavMenus\SocialNavWalker();

$mainNav = array(
	'theme_location'  => 'social-navigation',
	'menu'            => '',
	'container'       => '',
	'container_class' => 'social-navigation',
	'container_id'    => '',
	'menu_class'      => 'nav-list -social',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>',
	'depth'           => 0,
	'walker'          => $socialWalker
);

wp_nav_menu( $mainNav );
?>
