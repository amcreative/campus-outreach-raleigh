<nav id="js-main-navigation" class="main-navigation" role="navigation" aria-label="Main menu" aria-hidden="true">

	<?php get_template_part( 'views/Layouts/Global/Navigation/MainNavigation' ); ?>

	<?php get_template_part( 'views/Layouts/Global/Navigation/SocialNavigation' ); ?>

</nav>
