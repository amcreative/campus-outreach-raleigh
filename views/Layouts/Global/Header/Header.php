<?php
global $post;

get_template_part( 'views/Layouts/Global/Navigation/Navigation' );

$postId = $post->ID;
$bkgImg = '';
$title = '';
$cssClass = 'site-header';

if ( has_post_thumbnail( $postId ) && ! is_post_type_archive( 'staff' ) ) {
	$bkgImg = wp_get_attachment_image_src( get_post_thumbnail_id( $postId ), 'full-width-hero' )[0];
} elseif ( is_post_type_archive( 'staff' ) ) {
	$bkgImg = trailingslashit( CO_RALEIGH_URL ) . 'dist/images/summer-project-2017.jpg';
} else {
	$bkgImg = trailingslashit( CO_RALEIGH_URL ) . 'dist/images/summer-project-2017.jpg';
}

if ( is_front_page() ) {
	$title = get_bloginfo( 'description' );
	$cssClass = 'site-header -frontpage';
} elseif ( is_post_type_archive( 'staff' ) ) {
	$title = 'Campus Outreach Raleigh Staff';
} else {
	$title = get_the_title( $postId );
}

?>
<header id="js-site-header" class="<?php echo esc_attr( $cssClass ); ?>" role="banner" style="background-image: url(<?php echo esc_url( $bkgImg ); ?>">
	<div id="js-sticky-header" class="container-row -fstart -fullwidth">
		<button id="js-navigation-toggle-button" class="navigation-button" aria-expanded="false">
			<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'co-raleigh' ); ?></span>
			<?php inline_svg( 'menu' ); ?>
		</button>
		<div class="site-logo">
			<a class="logo-link" href="<?php echo esc_url( site_url() ); ?>" rel="home"><?php inline_svg( 'campus-outreach-raleigh-logo' ); ?><span class="screen-reader-text"><?php esc_html_e( 'Campus Outreach Raleigh', 'co-raleigh' ); ?></span></a>
		</div>
		<button id="js-search-toggle-button" class="search-button" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Search', 'co-raleigh' ); ?></span><?php inline_svg( 'search' ); ?></button>
	</div>
	<div id="js-hero-container" class="container-column">
		<h1 class="hero-title"><?php echo esc_html( $title ); ?></h1>
		<?php if ( is_singular( 'post' ) ) { ?>
			<div class="entry-meta -article"><?php getPostDate( $postId ); ?> <?php getPostCategory( $postId ); ?></div>
		<?php } ?>
	</div>
	<?php if ( is_front_page() ) { ?>
		<a class="scroll-button -down" data-scroll href="#main-content"><?php inline_svg( 'arrow-down' ); ?><span class="screen-reader-text"><?php esc_html_e( 'Scroll Down', 'co-raleigh' ); ?></span></a>
	<?php } ?>
</header>
