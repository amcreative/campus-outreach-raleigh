<?php
$bkgImg = 'https://coraleigh.local/wp-content/uploads/2018/08/summer-project-2017.jpg';
$title = get_bloginfo( 'description' );
?>
<nav id="js-main-navigation" class="main-navigation" role="navigation" aria-label="Main menu">
	<?php

	$navWalker = new \CORaleigh\Modules\NavMenus\NavWalker();

	$mainNav = array(
		'theme_location'  => 'main-navigation',
		'menu'            => '',
		'container'       => 'false',
		'container_class' => 'main-navigation',
		'container_id'    => '',
		'menu_class'      => 'nav-list',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>',
		'depth'           => 0,
		'walker'          => $navWalker
	);

	wp_nav_menu( $mainNav );
	?>
	<button id="js-donation-button" class="button -secondary" aria-pressed="false"><?php esc_html_e( 'Donate', 'co-raleigh' ); ?></button>
	<ul class="social-navigation">
		<li class="nav-item">
			<a class="nav-link" href=""></a>
		</li>
	</ul>
</nav>
<header id="js-site-header" class="site-header" role="banner" style="background-image: url(<?php echo esc_url( $bkgImg ); ?>">
	<div class="container-row">
		<button id="js-navigation-toggle-button" class="navigation-button" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Menu', 'co-raleigh' ); ?></span><?php inline_svg( 'menu' ); ?></button>
		<div class="site-logo">
			<a class="logo-link" href="<?php echo esc_url( site_url() ); ?>" rel="home"><?php inline_svg( 'campus-outreach-raleigh-logo' ); ?><span class="screen-reader-text"><?php esc_html_e( 'Campus Outreach Raleigh', 'co-raleigh' ); ?></span></a>
		</div>
		<button id="js-search-toggle-button" class="search-button" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Search', 'co-raleigh' ); ?></span><?php inline_svg( 'search' ); ?></button>
	</div>
	<div class="container-column">
		<h1 class="hero-title"><?php echo esc_html( $title ); ?></h1>
	</div>
	<button class="scroll-button -down" aria-pressed="false"><?php inline_svg( 'arrow-down' ); ?><span class="screen-reader-text"><?php esc_html_e( 'Scroll Down', 'co-raleigh' ); ?></span></button>
</header>
