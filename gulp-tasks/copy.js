import chalk from 'chalk';
import gulp from 'gulp';
import livereload from 'gulp-livereload';

const log = console.log,
  nodeDir = './node_modules',
  cssOpts = {
    dest: './src/css/vendor',
    vendorFiles: `${nodeDir}/normalize.css/normalize.css`
  },
  compiledDir = './src/js/compiled',
  jsOpts = {
    srcDest: './dist/js',
    srcFiles: [
      `${compiledDir}/build.js`,
      `${compiledDir}/build.js.map`
    ],
    vendorDest: './dist/js/vendor',
    vendorFiles: [
      `${nodeDir}/web-animations-js/web-animations-next.min.js`,
      `${nodeDir}/web-animations-js/web-animations-next.min.js.map`
    ]
  };

/**
 * Function to concat all files in a src directory.
 *
 * @author  Allen Moore
 * @param   {Object}   atts an Object of file properties.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('copy', () => {
  log(chalk.green('--- Copying JS Src Files ---'));

  return gulp.src(jsOpts.srcFiles)
    .pipe(gulp.dest(jsOpts.srcDest))
    .pipe(livereload());
});

/**
 * Function to concat all files in a src directory.
 *
 * @author  Allen Moore
 * @param   {Object}   atts an Object of file properties.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('copy-vendor', () => {
  log(chalk.green('--- Copying JS Vendor Files ---'));

  return gulp.src(jsOpts.vendorFiles)
    .pipe(gulp.dest(jsOpts.vendorDest))
    .pipe(livereload());
});

/**
 * Function to concat all files in a src directory.
 *
 * @author  Allen Moore
 * @param   {Object}   atts an Object of file properties.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('copy-css', () => {
  log(chalk.green('--- Copying CSS Files ---'));

  return gulp.src(cssOpts.vendorFiles)
    .pipe(gulp.dest(cssOpts.dest))
    .pipe(livereload());
});
