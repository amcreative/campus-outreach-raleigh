<?php
/**
 * The template for displaying the header on staff profile pages.
 *
 * @package CORaleigh
 * @since 1.0.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	do_action( 'et_head_meta' );
	?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php get_template_part( 'views/Layouts/Global/Navigation/Navigation' ); ?>

	<header id="js-site-header" class="site-header -profile" role="banner">

		<div id="js-sticky-header" class="container-row -fstart -fullwidth -profile">

			<button id="js-navigation-toggle-button" class="navigation-button -profile" aria-expanded="false">
				<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'co-raleigh' ); ?></span>
				<?php inline_svg( 'menu' ); ?>
			</button>

			<div class="site-logo -profile">

				<a class="logo-link" href="<?php echo esc_url( site_url() ); ?>" rel="home">
					<?php inline_svg( 'campus-outreach-raleigh-logo' ); ?>
					<span class="screen-reader-text"><?php esc_html_e( 'Campus Outreach Raleigh', 'co-raleigh' ); ?></span>
				</a>
			</div>

			<button id="js-search-toggle-button" class="search-button -profile" aria-expanded="false">
				<span class="screen-reader-text"><?php esc_html_e( 'Search', 'co-raleigh' ); ?></span>
				<?php inline_svg( 'search' ); ?>
			</button>

		</div>

	</header>

	<main id="main-content" class="site-content">
