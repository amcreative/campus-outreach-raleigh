<?php get_header(); ?>

	<div id="main-content" class="site-content">

		<div class="container-row">
			<div id="content-area" class="clearfix">
				<div class="staff-list">
					<div class="team-list -filterall -filteret" data-fid="86" data-filter="filteret">
						<h2 class="team-title"><?php esc_html_e( 'Executive Team', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'executive-team' ); ?>
					</div>
					<div class="team-list -filterall -filtercu" data-fid="87" data-filter="filtercu">
						<h2 class="team-title"><?php esc_html_e( 'Campbell University', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'campbell-university' ); ?>
					</div>
					<div class="team-list -filterall -filterecu" data-fid="88" data-filter="filterecu">
						<h2 class="team-title"><?php esc_html_e( 'East Carolina University', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'east-carolina-university' ); ?>
					</div>
					<div class="team-list -filterall -filterncsu" data-fid="89" data-filter="filterncsu">
						<h2 class="team-title"><?php esc_html_e( 'NC State University', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'nc-state-university' ); ?>
					</div>
					<div class="team-list -filterall -filteruncch" data-fid="90" data-filter="filteruncch">
						<h2 class="team-title"><?php esc_html_e( 'University of North Carolina - Chapel Hill', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'university-of-north-carolina-chapel-hill' ); ?>
					</div>
					<div class="team-list -filterall -filtermobl" data-fid="91" data-filter="filtermobl">
						<h2 class="team-title"><?php esc_html_e( 'Mobilization', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'mobilization' ); ?>
					</div>
					<div class="team-list -filterall -filterro" data-fid="91" data-filter="filterro">
						<h2 class="team-title"><?php esc_html_e( 'Resource Center', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'resource-center' ); ?>
					</div>
					<div class="team-list -filterall -filtercs" data-fid="87" data-filter="filtercs">
						<h2 class="team-title"><?php esc_html_e( 'Contingent Staff', 'co-raleigh' ); ?></h2>
						<?php getAllStaffByTeam( 'contingent-staff' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
