<?php
/**
 * The template for displaying the header.
 *
 * @package CORaleigh
 * @since 1.0.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	do_action( 'et_head_meta' );
	?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php
	get_template_part( 'views/Layouts/Global/Navigation/Navigation' );

	while ( have_posts() ) : the_post();

		$eventId = get_the_ID();

		if ( has_post_thumbnail( $eventId ) ) {
			$bkgImg = wp_get_attachment_image_src( get_post_thumbnail_id( $eventId ), 'full-width-hero' )[0];
		} else {
			$bkgImg = trailingslashit( CO_RALEIGH_URL ) . 'dist/images/summer-project-2017.jpg';
		}

		$title = get_the_title( $eventId ); ?>

		<header id="js-site-header" class="site-header" role="banner" style="background-image: url(<?php echo esc_url( $bkgImg ); ?>">

			<div id="js-sticky-header" class="container-row -fstart -fullwidth">
				<button id="js-navigation-toggle-button" class="navigation-button" aria-expanded="false">
					<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'co-raleigh' ); ?></span>
					<?php inline_svg( 'menu' ); ?>
				</button>

				<div class="site-logo">
					<a class="logo-link" href="<?php echo esc_url( site_url() ); ?>" rel="home"><?php inline_svg( 'campus-outreach-raleigh-logo' ); ?><span class="screen-reader-text"><?php esc_html_e( 'Campus Outreach Raleigh', 'co-raleigh' ); ?></span></a>
				</div>

				<button id="js-search-toggle-button" class="search-button" aria-expanded="false">
					<span class="screen-reader-text"><?php esc_html_e( 'Search', 'co-raleigh' ); ?></span>
					<?php inline_svg( 'search' ); ?>
				</button>

			</div>

			<div id="js-hero-container" class="container-column">

				<?php the_title( '<h1 class="hero-title">', '</h1>' ); ?>

				<div class="entry-meta -event">
					<?php getEventDates( $eventId, 'event' ); ?>

					<?php getEventLocation( $eventId, 'event' ); ?>

					<?php if ( tribe_get_cost() ) : ?>
						<span class="event-cost"><?php echo tribe_get_cost( null, true ) ?></span>
					<?php endif; ?>

				</div>

			</div>

		</header>

	<?php endwhile; ?>

	<main id="main-content" class="main-content" role="main">

