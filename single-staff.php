<?php get_header( 'staff-profile' ); ?>

	<div class="container-row">
		<div id="content-area" class="clearfix">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$id = get_the_ID();
					$givingUrl = get_post_meta( $id, 'staff_giving_url' )[0];
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'staff-headshot' )[0];
					$link = get_the_permalink( $id );
					$name = get_the_title( $id );
					$team = get_the_terms( $id, 'staff-team' );
					$title = get_post_meta( $id, 'staff_title' )[0];
					?>
					<div class="staff-member -profile">
						<div class="staff-meta -profile">
							<h1 class="staff-name -profile"><a class="link" href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( 'Permalink to: Staff page of ' . $name ); ?>"><?php echo esc_html( $name ); ?></a></h3>
							<h2 class="staff-title -profile"><?php echo esc_html( $title ); ?></h4>
						</div>
						<div class="staff-image -profile">
							<img class="image" src="<?php echo esc_url( $image ); ?>">
						</div>
						<div class="staff-content">
							<?php echo sprintf( '<iframe id="js-giving-iframe" class="iframe" src="%1$s"></iframe>', esc_url( 'https://app.mogiv.com/give/?cid=coraleigh.' . $givingUrl .'&frameless=1&custom=1&includeLearnMore=0' ) ); ?>
						</div>
					</div>
					<?php
				endwhile;
			endif;
			?>
		</div>
	</div>

<?php get_footer();
